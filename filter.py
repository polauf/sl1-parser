import cv2 as cv
import open3d as o3d
from typing import Union, Any
import glob, re
import os, argparse
import sys
import numpy as np
from zipfile import ZipFile
from configparser import ConfigParser
from io import BytesIO
from PIL import Image

def duck_cast(value: str) -> Union[int, float]:
    "Cast value to int or float."
    try:
        return int(value)
    except ValueError:
        return float(value)

def load_config(data: bytes) -> dict:
    peaces = [ p.split('=', maxsplit=1) for p in data.decode().strip().split('\n')]
    # Don't trust invalid ini file expecting space and characters inconsistency
    # Simple faster way
    # return {v[0].strip(): v[1].strip() for v in config if len(v) > 1}
    config = {}
    for v in peaces:
        if len(v) > 1:
            try:
                config[v[0].strip()] = duck_cast(v[1])
            except ValueError:
                config[v[0].strip()] = v[1].strip()
    return config

def make_edited_images(images: dict, transform_func=None, func_args=()) -> dict:
    new_images = {}
    for h,(k,i) in enumerate(images.items()):
        # drop junk
        i[i > 0] = 255
        new = np.full(i.shape, 0, np.uint8)
        if transform_func is not None:
            new = transform_func(i, new, *func_args)
        new_images[k] = cv.imencode('.png', new)[1]
    return new_images

def save_sl1(source:str, destination:str, images: dict) -> None:
    try:
        os.remove(destination)
    except FileNotFoundError:
        pass
    with ZipFile(destination, 'w') as dest:
        for n,i in images.items():
            dest.writestr(n,i)
        with ZipFile(source, 'r') as src:
            for fn in src.filelist:
                if not fn.filename in dest.namelist():
                    dest.writestr(fn.filename, src.read(fn.filename))

# FILTERS

def simplify(img, new, amount=0.005):
    approx = []
    contours, _ = cv.findContours(img, cv.RETR_LIST, cv.CHAIN_APPROX_SIMPLE)
    for c in contours:
        e = amount*cv.arcLength(c, True)
        approx.append(cv.approxPolyDP(c, e, True))
    return cv.drawContours(new, approx, -1, 255, -1)

def rectanglefy(img, new):
    approx = []
    contours, _ = cv.findContours(img, cv.RETR_LIST, cv.CHAIN_APPROX_SIMPLE)
    for c in contours:
        # everithing is rectangle now
        x, y, w, h = cv.boundingRect(c)
        approx.append(np.array([[[x,y], [x,y+h], [x+w,y+h], [x+w,y]]]))
    return cv.drawContours(new, approx, -1, 255, -1)

def smooth(img, new, kernel=(5,5), iterations=1):
    kernel = np.ones(kernel, np.uint8)
    # classic erode dilate but crappy for small objects. It will drop them.
    new = cv.erode(img, kernel, iterations = iterations)
    new = cv.dilate(new, kernel, iterations = iterations)
    return new

def gauss_smooth(img, new, kernel=(5,5), sigma = 1):
    new = cv.GaussianBlur(img, kernel,sigma)
    # Blur have range so drop it +- between
    new[new > 122] = 255
    new[new <= 122] = 0
    return new

def mess_up_edges(img, new, dept=20, mean=100, sd=100):
    contours, _ = cv.findContours(img, cv.RETR_LIST, cv.CHAIN_APPROX_SIMPLE)
    new = cv.drawContours(new, contours, -1, 255, 2*dept)
    # make some noise
    gaussian_noise = np.zeros(img.shape,dtype=np.uint8)
    cv.randn(gaussian_noise, mean, sd)
    # add nose to the contours
    new = new * gaussian_noise
    # add original image
    new = new + img
    # drop middle (junk)
    new[new < 122] = 0
    new[new >= 122] = 255
    # remove noise from where wasn't any layer 
    return new - cv.bitwise_not(img)

filters = {f.__name__: f for f in [simplify, rectanglefy, smooth, gauss_smooth, mess_up_edges]}


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description="Filtering program for .sl1 files. (Everithing is done in RAM, so you need RAM).")
    parser.add_argument('-s', '--source', required=True, help="Input file in format .sl1")
    parser.add_argument('-o', '--output', required=True, help="Output file.")
    # TODO: More than one filter (Don't want to mess with connected args parameter.)
    parser.add_argument('-f', '--filter', choices=filters.keys(), help="Choose a filter.")
    parser.add_argument('-a', '--args', help="Arguments for chosen filter for example '(1,(5,5))' (see function signature).")
    ns = parser.parse_args()

    # TODO: Nicer Error messages for exceptions like here if file not exist.
    zip_file = ZipFile(ns.source)
    config = load_config(zip_file.read('config.ini'))

    images = {}
    for f in zip_file.filelist:
        if config['jobDir'] in f.filename:
            # TODO: not used but usefull (Re Overkill)
            info = re.match(f"{config['jobDir']}(.+)\.(.+)", f.filename)
            if hasattr(info, 'groups'):
                arr = np.frombuffer(zip_file.read(f.filename),np.uint8)
                images[f.filename] = cv.imdecode(arr, cv.IMREAD_GRAYSCALE)
    print(f"File {ns.source} loaded.")

    # more filters
    if ns.filter in filters:
        args = []
        if ns.args:
            for a in ns.args[1:-1].split(","):
                try:
                    args.append(duck_cast(a))
                except ValueError:
                    args.append(a.strip())
        print(f"Args for filter: {args}")
        images = make_edited_images(images, filters[ns.filter], args)
        print(f"Filter {ns.filter} applied.")

    save_sl1(ns.source, ns.output, images)
    print(f"File {ns.output} saved.")