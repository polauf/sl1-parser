# SL1 parser

Simple SL1 parser and applier of filters.

Yu can use it for example:
```bash
python filter.py -s 'your file.sl1' -o out.sl1 -f simplify -a '(0.1)'
```

## Help

```
usage: filter.py [-h] -s SOURCE -o OUTPUT
                 [-f {simplify,rectanglefy,smooth,gauss_smooth,mess_up_edges}]
                 [-a ARGS]

Filtering program for .sl1 files. (Everithing is done in RAM, so you need
RAM).

optional arguments:
  -h, --help            show this help message and exit
  -s SOURCE, --source SOURCE
                        Input file in format .sl1
  -o OUTPUT, --output OUTPUT
                        Output file.
  -f {simplify,rectanglefy,smooth,gauss_smooth,mess_up_edges}, --filter {simplify,rectanglefy,smooth,gauss_smooth,mess_up_edges}
                        Choose a filter.
  -a ARGS, --args ARGS  Arguments for chosen filter for example '(1,(5,5))'
                        (see function signature).

```

Released under modified **WTFPL**.
